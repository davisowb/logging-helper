import os
import logging_tree
from logging_helper import logging_helper


logtest = logging_helper.setup_logging(logger_name=u'TESTING HELPER', level=logging_helper.DEBUG, log_to_file=True)


# TODO: Add these to unittests!
logtest.debug(u'Test log message 1')
logtest.lh_set_console_level(logging_helper.INFO)
logtest.debug(u'Test log message 2')  # This message should not display
logtest.lh_set_console_level(logging_helper.DEBUG)
logtest.debug(u'Test log message 3')
logtest.setLevel(logging_helper.INFO,
                 logtest.LH_CONSOLE_HANDLER)  # this is the same as logtest.lh_set_console_level(INFO)
logtest.debug(u'Test log message 4')  # This message should not display
logtest.setLevel(logging_helper.DEBUG,
                 logtest.LH_CONSOLE_HANDLER)  # this is the same as logtest.lh_set_console_level(DEBUG)
logtest.debug(u'Test log message 5')
logtest.setLevel(logging_helper.WARN)  # This sets the root logger level
logtest.debug(u'Test log message 6')  # This message should not display
logtest.setLevel(logging_helper.DEBUG)
logtest.debug(u'Test log message 7')
logtest.LH_FILE_HANDLER.set_path(u'{p}{sep}{d}'.format(p=logging_helper.gettempdir(),
                                                       sep=os.sep,
                                                       d=u'logs2'))
logtest.debug(u'Test log message 8')
logtest.debug(u'Test log message 9')

logtest.info(u'Test line')
logtest.info(u'Multi\nLine\nTest')

logging_tree.printout()
